<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'twentyfifteen' ),
		'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = twentyfifteen_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentyfifteen_fonts_url() ) );
}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'twentyfifteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

if ( ! function_exists( 'twentyfifteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentyfifteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function twentyfifteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyfifteen_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function twentyfifteen_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'twentyfifteen-style', $css );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function twentyfifteen_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';


// Custom Post - Feedbacks

add_action( 'init', 'Create_Post_Feedbacks' );
function Create_Post_Feedbacks() {
  register_post_type( 'feedbacks',
    array(
      'labels' => array(
        'name' => __( 'Feedbacks' ),
      ),
      'public' 			=> true,
      'menu_icon'		=> 'dashicons-testimonial',
      'menu_position'	=> 5,
      'supports' => array('title'),
    )
  );
}

//Remove submenu from Feedbacks
add_action('admin_menu', 'remove_submenu_feedback');
function remove_submenu_feedback($submenu) {
    global $submenu;
    unset($submenu['edit.php?post_type=feedbacks']);
    return $submenu;
}


function Feedback_Scripts() {
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js');
	wp_enqueue_script('validator', get_template_directory_uri().'/js/jquery.form-validator.min.js');
	wp_enqueue_script('feedback', get_template_directory_uri().'/js/feedback.js');
	wp_localize_script('feedback', 'feedback', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )) );

}
add_action('wp_enqueue_scripts', 'Feedback_Scripts');




// Save feedback from ajax request
function feedback_save_callback() {

    if ( isset($_REQUEST) ) {

        $name = sanitize_text_field( $_REQUEST['name'] );
        $email = sanitize_text_field( $_REQUEST['email'] );
  		$comment = sanitize_text_field( $_REQUEST['comment'] );

  		$post_type = 'feedbacks';

  		$post = array(
  			'post_title' 	=> 'Feedback',
  			'post_type'		=> $post_type
  			);


  		$dublicate = feedback_dublicate($name, $email, $comment);

  		//If exist all values AND Post not dublicate
  		if ( ($name && $comment && is_email($email)) AND ($dublicate == false) ) {

  			$post_id = wp_insert_post($post);

  			add_post_meta($post_id, 'name', $name, true);
  			add_post_meta($post_id, 'email', $email, true);
  			add_post_meta($post_id, 'comment', $comment, true);

  			$update_title = array(
  				'ID' => $post_id, 
  				'post_title' => 'Feedback - '.$post_id
  				);

  			wp_update_post( $update_title );

  			echo 'Saved: '.$post_id;

  		} else {

  			if ($dublicate == true) {
  				echo 'Exist';
  			} else {
	  			echo 'Error';
  			}

  		}
    }
     
   die();
}
 
add_action( 'wp_ajax_feedback_save', 'feedback_save_callback' );
add_action( 'wp_ajax_nopriv_feedback_save', 'feedback_save_callback' );


function feedback_dublicate($name, $email, $comment, $post_type = 'feedbacks') {

  		$find_dublicate = array(
  			'post_type' => $post_type,
  			'meta_query' => array(
  				'relation' => 'AND',
  				array(
  					'key' 		=> 'name',
  					'value' 	=> $name,
  					'compare' 	=> 'LIKE'
  					),
  				array(
  					'key' 		=> 'email',
  					'value' 	=> $email,
  					'compare' 	=> 'LIKE'
  					),
  				array(
  					'key' 		=> 'comment',
  					'value' 	=> $comment,
  					'compare' 	=> 'LIKE'
  					),
  				)
  			);

  		$dublicate = new WP_Query( $find_dublicate );

  		if ( $dublicate->have_posts() ) {
  			return true;
  		} else {
  			return false;
  		}
}

//Add export menu in feedbacks list

add_action( 'admin_footer-edit.php', 'add_export_menu' );

function add_export_menu() 
{
	global $post_type;
	
	if ($post_type == 'feedbacks') : ?>

		    <script type="text/javascript">
		        jQuery(document).ready(function($) {
		            $('<option>').val('export_xls').text('Export XLS')
		                .appendTo("select[name='action'], select[name='action2']");
		        });
		    </script>

    <?php endif;
}



//Export to XLS function

add_action( 'admin_action_export_xls', 'export_feedbacks_xls' );


function export_feedbacks_xls() 
{
	$url = 'inc/PHPExcel.php';
    $posts = $_REQUEST['post'];
 	
 	require_once $url;

	$Feedbacks_Data = new PHPExcel();

	$Feedbacks_Data->setActiveSheetIndex(0);

	//Set size for each column
	$Feedbacks_Data->getActiveSheet()->getColumnDimension('A')->setWidth('8');
	$Feedbacks_Data->getActiveSheet()->getColumnDimension('B')->setWidth('25');
	$Feedbacks_Data->getActiveSheet()->getColumnDimension('C')->setWidth('30');
	$Feedbacks_Data->getActiveSheet()->getColumnDimension('D')->setWidth('50');


	//Align left top
	$Feedbacks_Data->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
	$Feedbacks_Data->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

	$Feedbacks_Data->getActiveSheet()->mergeCells('A1:C1');
	$Feedbacks_Data->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(20);
	$Feedbacks_Data->getActiveSheet()->SetCellValue('A1', 'Feedbacks Data');


	$Feedbacks_Data->getActiveSheet()->getRowDimension('2')->setRowHeight(40);

	$Feedbacks_Data->getActiveSheet()->SetCellValue('A2', 'ID');
	$Feedbacks_Data->getActiveSheet()->SetCellValue('B2', 'Name');
	$Feedbacks_Data->getActiveSheet()->SetCellValue('C2', 'Email');
	$Feedbacks_Data->getActiveSheet()->SetCellValue('D2', 'Comment');


	$i = '4';
	foreach ($posts as $post) {

		$name 		= get_post_meta( $post, 'name', true );
		$email 		= get_post_meta( $post, 'email', true );
		$comment 	= get_post_meta( $post, 'comment', true );

		$Feedbacks_Data->getActiveSheet()->SetCellValue('A'.$i, $post);
		$Feedbacks_Data->getActiveSheet()->SetCellValue('B'.$i, $name);
		$Feedbacks_Data->getActiveSheet()->SetCellValue('C'.$i, $email);
		$Feedbacks_Data->getActiveSheet()->SetCellValue('D'.$i, $comment);
		
		unset($name, $email, $comment);
		$i++;
	}

	$Feedbacks_Data->getActiveSheet()->getStyle('A1:D'.$i)->getAlignment()->setWrapText(true);
	
	$Feedbacks_Data->getActiveSheet()->setTitle('Feedbacks Title');


	$Write_Data = PHPExcel_IOFactory::createWriter($Feedbacks_Data, 'Excel5');

	$file_name = 'feedbacks_'.date('Y-m-d');
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$file_name.'.xls"');
	$Write_Data->save('php://output');

exit;
}

function feedback_list_columns( $column ) {
 	unset($column['date']);

 	$column['name'] = 'Name';
 	$column['email'] = 'Email address';
 	$column['comment'] = 'Comment';
 	$column['date'] = 'Date';

    return $column;
}
add_filter( 'manage_feedbacks_posts_columns', 'feedback_list_columns' );



function feedback_modify_column( $column, $post_id ) {
 
    $custom_fields = get_post_custom( $post_id );

    switch ($column) {
        case 'name' :
            echo $custom_fields['name'][0];
            break;
 
        case 'email' :
        	echo $custom_fields['email'][0];
            break;

        case 'comment' :

        	$comm = $custom_fields['comment'][0];

        	if (strlen($comm) > 150 ) {

        		echo substr($comm, 0, 150).'...';

        	} else {
        		echo $comm;
        	}

        	
            break;
        default:
    }
}
add_action( 'manage_feedbacks_posts_custom_column', 'feedback_modify_column', 10, 2 );

add_filter( 'manage_edit-feedbacks_sortable_columns', 'sort_feedback_collumns' );
function sort_feedback_collumns( $columns ) {
	$columns['name'] = 'name';
	$columns['email'] = 'email';
    $columns['comment'] = 'comment';

 
    return $columns;
}

function custom_admin_css() {
	wp_enqueue_style('admin_css_custom', get_template_directory_uri().'/css/admin_custom.css');
}
add_action( 'admin_enqueue_scripts', 'custom_admin_css' );



function feedback_add_meta_box() {
    add_meta_box('feedback_box','Feedbacks Edit', 'feedback_meta_box_callback', 'feedbacks', 'advanced', 'high');

}

add_action( 'add_meta_boxes', 'feedback_add_meta_box' );


function feedback_meta_box_callback( $post ) {

	wp_nonce_field( 'feedback_save_meta_box_data', 'feedback_meta_box_nonce' );

	$name 		= get_post_meta( $post->ID, 'name', true );
	$email 		= get_post_meta( $post->ID, 'email', true );
	$comment 	= get_post_meta( $post->ID, 'comment', true );
?>
	<div id="save_feedback">

		<div>
			<label for="name">Name</label>
			<input type="text" id="name" name="name" value="<?php echo esc_attr( $name ); ?>" size="25" />
		</div>
	
		<div>
			<label for="email">Email</label>
			<input type="text" id="email" name="email" value="<?php echo esc_attr( $email ); ?>" size="25" />
		</div>

		<div class="comment">
			<label for="comment">Comment</label>
			<textarea type="text" id="comment" name="comment" /><?php echo esc_attr( $comment ); ?></textarea>
		</div>

	</div>
	<?php
}


function feedback_save_meta_box_data( $post_id ) {

	if ( ! isset( $_POST['feedback_meta_box_nonce'] ) )  return; 

	if ( ! wp_verify_nonce( $_POST['feedback_meta_box_nonce'], 'feedback_save_meta_box_data' ) )  return; 

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  return; 

	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) )  return; 
	} else {
		if ( ! current_user_can( 'edit_post', $post_id ) )  return; 
	}

	$name 		= sanitize_text_field( $_POST['name'] );
	$email 		= sanitize_text_field( $_POST['email'] );
	$comment 	= sanitize_text_field( $_POST['comment'] );

	update_post_meta( $post_id, 'name', $name );
	update_post_meta( $post_id, 'email', $email );
	update_post_meta( $post_id, 'comment', $comment );
}

add_action( 'save_post', 'feedback_save_meta_box_data' );