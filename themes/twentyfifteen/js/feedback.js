(function($) {

$(document).ready(function(){


$('#feedbacks-form').validate({ 
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
            email: true
        },
        comment: {
            required: true,
        }
    },
    messages: {
        name: "Please enter your name",

        email: {
            required: "Please enter your email address",
            email: "Enter valid email address"
        },

        comment: "This field is required"
    },
    submitHandler: function(form) {

        save_feedback(form);

    }
});

function save_feedback(data) {

    var name  = $(data).find('#name').val();
    var email = $(data).find('#email').val();
    var comment = $(data).find('#comment').val();
     
    $.ajax({
        url: feedback.ajaxurl,
        data: {
            'action':'feedback_save',
            'name' : name,
            'email' : email,
            'comment' : comment
        },
        success:function(data2) {
            $(data).find("p").remove();

            if (data2 == 'Exist') {
                $('<p class="Feedmsg error">Exist</p>').hide().prependTo(data).show('normal');
            } else {
                $('<p class="Feedmsg">Feedback saved</p>').hide().prependTo(data).show('normal');
            }
            
            console.log(data2);
            
        },
        error: function(errorThrown){
            console.log(errorThrown);
            console.log('Error');
        }
    });  
}

// Email validator
// $('#email').keyup(function() {
//     var email = $(this).val();
//     var regExp = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

//     //Add or remove error message
//     if (!regExp.test(email) && !$('.error').length) {

//         $(this).parent('div').addClass('has-error');
//         $(this).before('<span class="error fr">Please enter valid email address</span>');
//         $('button').attr('disabled', 'disabled');


//     } else if (regExp.test(email) && $('.error').length) {

//         $(this).parent('div').removeClass('has-error');
//         $('.error').remove();
//         $('button').removeAttr('disabled');
//     }

// });
})
})( jQuery );