<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


				<div class="entry-content">


					<form action="/test" method="post" id="feedbacks-form">

					<p style="display: none">Hidden</p>

						<fieldset>

						<legend>Feedback</legend>
						
						<div class="form-group">
							<label for="name">Name</label>
							<input id="name" name="name" placeholder="Name" class="form-control">
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input id="email" name="email" placeholder="Email address" class="form-control" data-validation="email">
						</div>

						<div class="form-group comment">
							<label for="comment">Comment</label>
							<textarea id="comment" name="comment" class="form-control" rows="5"></textarea>
						</div>
						
						<button type="submit" class="btn btn-primary">Submit</button>

						</fieldset>

					</form>

				</div><!-- .entry-content -->


			</article><!-- #post-## -->




		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
